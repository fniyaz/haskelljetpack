module Obstacles where

import Collectable
import Geometry
import Graphics.Gloss


data Obstacle = LazerObstacle Lazer
              | CollectableObstacle Collectable
              deriving (Eq)

fromCollectable :: Collectable -> Obstacle
fromCollectable = CollectableObstacle


-- | Move obstacle given offsets and obstacle to move
moveObstacle :: (Float, Float) -> Obstacle -> Obstacle
moveObstacle (offsetX, offsetY) (LazerObstacle (Lazer (px1, py1) (px2, py2))) =
  LazerObstacle (Lazer (px1 + offsetX, py1 + offsetY) (px2 + offsetX, py2 + offsetY))
moveObstacle dX (CollectableObstacle coll) =
  CollectableObstacle $ moveCollectable dX coll

data Orientation = Horizontal | Vertical
  deriving (Eq)

-- centers of circles, lenght of connecting line btw 2 circles
data Lazer = Lazer Point Point
  deriving (Eq)


-- radius of circle and width of connecting line btw 2 circles
lazerRadius :: Float
lazerWidth :: Float
(lazerRadius, lazerWidth) = (5, 5)

-- | Create a Lazer Obstacle between two points in the world
createLazer :: Point -> Point -> Obstacle
createLazer p1 p2 = LazerObstacle (Lazer p1 p2)

-- get collider of an obstacle
getObstacleCollider :: Obstacle -> Collider
getObstacleCollider (LazerObstacle lazer) = getLazerCollider lazer
getObstacleCollider (CollectableObstacle coll) = getCollectableCollider coll

-- get collider of lazer
getLazerCollider :: Lazer -> Collider
getLazerCollider (Lazer (px1, py1) (px2, py2)) = BoxCollider (x1, y1) (x2, y2) 0
  where
    ((x1, y1), (x2, y2)) = getColliderCoords ((px1, py1), (px2, py2)) lazerRadius

-- calculates left down and right up points for collider
getColliderCoords :: (Point, Point) -> Float -> (Point, Point)
getColliderCoords (p1@(px1, py1), p2@(px2, py2)) colliderRadius
  | (getOrientation p1 p2) == Vertical && py1 < py2 = firstPointEarlier
  | (getOrientation p1 p2) == Horizontal && px1 < px2 = firstPointEarlier
  | otherwise = secondPointEarlier
  where
    firstPointEarlier = ((px1 - colliderRadius, py1 - colliderRadius), (px2 + colliderRadius, py2 + colliderRadius))
    secondPointEarlier = ((px2 - colliderRadius, py2 - colliderRadius), (px1 + colliderRadius, py1 + colliderRadius))

-- for now there are only Vertical and Horizontal lazers
getOrientation :: Point -> Point -> Orientation
getOrientation (px1, _py1) (px2, _py2)
  | px1 == px2 = Vertical
  | otherwise = Horizontal

-- draw obstacle
drawObstacle :: Obstacle -> Picture
drawObstacle (LazerObstacle lazer) = drawLazer lazer
drawObstacle (CollectableObstacle coll) =
  drawCollectable coll

-- draw lazer
drawLazer :: Lazer -> Picture
drawLazer (Lazer (px1, py1) (px2, py2)) =
  (translate px1 py1 lazerPoint)
    <> (translate px2 py2 lazerPoint)
    <> (color orange $ lazerLight orientation)
  where
    lazerPoint = color yellow $ circleSolid lazerRadius
    orientation = getOrientation (px1, py1) (px2, py2)
    lazerLight Vertical =
      polygon
        [ (px1 - lazerWidth / 2, py1),
          (px1 + lazerWidth / 2, py1),
          (px2 - lazerWidth / 2, py2),
          (px2 + lazerWidth / 2, py2)
        ]
    lazerLight Horizontal =
      polygon
        [ (px1, py1 - lazerWidth / 2),
          (px1, py1 + lazerWidth / 2),
          (px2, py2 - lazerWidth / 2),
          (px2, py2 + lazerWidth / 2)
        ]

updateObstacle :: Float -> Obstacle -> Obstacle
updateObstacle _dt = id
