module Game (runGame) where

import           Graphics.Gloss
import           Graphics.Gloss.Interface.IO.Game
import           Graphics.Gloss.Interface.Environment
import           CommonDraws

import           Control.Monad.Random
import           Data.Function ((&))
import           Data.List (partition)

import qualified Player
import qualified World
import           Obstacles
import           Screen
import           Geometry

import           GlobalStateHandlers


-- * States

data GlobalState = GlobalState 
             { playerState :: Player.PlayerState
             , worldState  :: World.WorldState
             -- | the speed at which the game runs
             , speed       :: Float
             -- | travaled so far distance
             , distance    :: Float
             -- | collected coins
             , coins       :: Int
             -- | If the player is still alive
             , alive       :: Bool
             -- | If the debug mode is enabled
             , showDebug   :: Bool
             , debugInfo   :: DebugInfo
             }

data DebugInfo = DebugInfo 
            { -- | If the player is currently colliding with an obstacle
              d_isColliding :: Bool,
              -- | Current fps
              d_fps         :: Float
            }


-- | Generates the starting state for the game
initial :: (RandomGen g) => Rand g GlobalState
initial = 
  do
    initialWorld <- World.initWorld
    return $ GlobalState { playerState = Player.initPlayer
                      , worldState  = initialWorld
                      , speed       = 350 
                      , distance    = 0
                      , coins       = 0
                      , alive       = True
                      , showDebug   = False
                      , debugInfo   = emptyDebugInfo 
                      }

emptyDebugInfo :: DebugInfo 
emptyDebugInfo = DebugInfo 
            { d_isColliding = False
            , d_fps         = 0
            }


-- | How fast is the game running now
acceleration :: Float
acceleration = 8

-- * Logics

-- | Handles controll input
-- and delegates it to specific handlers
controllHandler :: Key -> KeyState -> GlobalState -> GlobalState
controllHandler button polarity global 
          = global 
          { playerState = applyPlayerInput 
              (toPlayerInput button polarity)
              (playerState global)
          } 
  where
    -- Map the controll to PlayerInput type
    toPlayerInput (SpecialKey KeyTab) Down      
          = Just (Player.PlayerShield)
    toPlayerInput (SpecialKey KeySpace) polarity' 
          = Just (Player.PlayerFly (polarity' == Down))
    toPlayerInput _ _           = Nothing

    -- Delegate it to the player handler
    applyPlayerInput :: Maybe Player.PlayerInput -> Player.PlayerState -> Player.PlayerState
    applyPlayerInput (Just input) = Player.recieveControll input 
    applyPlayerInput Nothing      = id


-- | Checks for collisions between world objects and reacts appropriately
applyCollisions :: GlobalState -> GlobalState
applyCollisions global = global  
    { alive = not hasDied
    , debugInfo = debug {
        d_isColliding = anyCollisions
      }
    , worldState = World.removeObstacles collected $ worldState global
    , coins = length collected + coins global
  }
  where
    debug = debugInfo global

    pcollider = Player.getCollider (playerState global)
    obstacles = World.getObstacleColliders(worldState global)

    collisions = filter (\((_, coll), player) -> colliding coll player)
                       (zip obstacles (repeat pcollider))
    (collected, obs) = partition isCollectable (map (fst . fst) collisions)
    isCollectable (CollectableObstacle _) = True
    isCollectable _                       = False

    anyCollisions = not $ null obs

    shields = Player.isShieldUp (playerState global)
    hasDied = anyCollisions && not shields && not (showDebug global)


-- | The global input handler
handleInput :: Event -> GlobalState -> GlobalState
handleInput (EventKey key keyState _ _) global
    = controllHandler key keyState (toggleDebug key keyState global)
  where
    toggleDebug (Char 'q') Down state 
        = state { showDebug = not (showDebug state) }
    toggleDebug _ _ state          = state
handleInput _ state                = state


-- | The global time pass handler
handleTime :: Float -> GlobalState -> GlobalState
handleTime dt global = applyCollisions $ global
                                    { playerState = Player.updatePlayer dt (playerState global)
                                    , worldState  = World.updateWorld newSpeed dt (worldState global)
                                    , debugInfo   = updatedDebug
                                    , speed       = newSpeed
                                    , distance    = newSpeed * dt / 100 + distance global
                                    }
  where
    newSpeed = (speed global) + acceleration * dt

    debug = debugInfo global
    updatedDebug = debug {
      d_fps = d_fps debug / 2 + 1 / dt / 2
    }


-- * Drawing

drawUI :: GlobalState -> Picture
drawUI global = drawMenu white
                      [ "distance: " ++ show (floor (distance global))
                      , "coins: " ++ show (coins global)
                      ]
             <> shieldBar
  where
    shieldBar = (color white outer
                <> color black inner)
              & translate 0 (screenMaxY - 10)

    shieldBarWidth = screenWidth/2
    outer = rectangleSolid (shieldBarWidth + 5) 15
    inner = rectangleSolid (shieldBarWidth * shield/maxShield) 10

    maxShield = Player.getMaxShieldReload (playerState global)
    shield = maxShield - Player.getCurrentShieldReload (playerState global)


drawDebug :: GlobalState -> Picture 
drawDebug global
  | showDebug global = debugColliders 
                     <> drawMenu (dark white) [collidingText, fpsText]
  | otherwise        = blank
  where
    debug = debugInfo global

    collidingText = if d_isColliding debug 
                            then "Crash"
                            else "No crash"
    fpsText = show $ d_fps debug

    debugColliders = color red playerCollider
                  <> color red obsCol

    playerCollider = drawCollider (Player.getCollider $ playerState global)
    obsCol = pictures $ map drawCollider obs
    obs = snd
        $ unzip 
        $ World.getObstacleColliders (worldState global)


-- | The global draw function
draw :: GlobalState -> Picture
draw global =  World.drawWorld (worldState global)
            <> color white (Player.draw (playerState global))
            <> drawUI global
            <> drawDebug global


-- | gameLoop
runGame :: IO ()
runGame = do
        (start:tryAgain) <- evalRandIO $ sequence $ repeat initial

        -- handling different screen sizes
        actualScreenSize <- getScreenSize
        let scaleX = (fromIntegral (fst actualScreenSize))
                      / screenWidth
        let scaleY = (fromIntegral (snd actualScreenSize))
                      / screenHeight

        -- run the game fullscreen
        ( play FullScreen bgColor fps
          -- scaling to fil the screen
          & withScaledDraw scaleX scaleY
          -- and with death and statistics
          & withDeathStats alive tryAgain (\a -> 
                              [ "Travaled: " ++ (show $ floor $ distance a)
                              , "Coins collected: " ++ (show $ coins a)
                              ]
                    )
          )
                  start
                  draw
                  handleInput
                  handleTime
  where
    bgColor = white
    fps = 60
