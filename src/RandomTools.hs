module RandomTools where

import Control.Monad.Random


-- | Wraps around a Rand monad splittind the incoming generator
-- to allow infinit list randomization via chewing up the generator
wrapSplit :: (RandomGen g) => Rand g a -> Rand g a
wrapSplit rand = liftRand evalSplit
  where
    evalSplit gen = (evalRand rand g, g')
      where
        (g, g') = split gen
