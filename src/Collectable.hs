module Collectable where

import Screen
import Geometry
import Graphics.Gloss

import Data.Function ((&))


data Collectable = CollectableCoin Coin
  deriving (Eq)


coinRadius :: Float
coinRadius = screenHeight / 30

data Coin = Coin Float Float
  deriving (Eq)


coinAt :: (Float, Float) -> Collectable
coinAt (x, y) = CollectableCoin (Coin x y) 


getCollectableCollider :: Collectable -> Collider
getCollectableCollider (CollectableCoin coin) =
  getCoinCollider coin

getCoinCollider :: Coin -> Collider
getCoinCollider (Coin x y) = CircleCollider (x, y) coinRadius


drawCollectable :: Collectable -> Picture
drawCollectable (CollectableCoin coin) = drawCoin coin

drawCoin :: Coin -> Picture
drawCoin (Coin x y) = (  color (dark yellow) (circleSolid coinRadius)
                      <> color yellow (circleSolid (0.8 * coinRadius))
                      ) 
                    & translate x y 


moveCollectable :: (Float, Float) -> Collectable -> Collectable
moveCollectable dX (CollectableCoin coin) = CollectableCoin $ moveCoin dX coin

moveCoin :: (Float, Float) -> Coin -> Coin
moveCoin (dx, dy) (Coin x y) = Coin (x+dx) (y+dy)
