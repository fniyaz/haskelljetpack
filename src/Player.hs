module Player where

import Geometry
import Graphics.Gloss
import Screen

data PlayerState = PlayerState
  { -- | coordinates of the player
    point :: Point,
    -- | time remaining shield duration
    shieldTime :: Float,
    -- | time before activation of shield
    shieldCooldown :: Float,
    -- | is player currently flying or not
    isFlying :: Bool,
    -- | range ["-maxSpeedUp","maxSpeedUp"],
    -- continious flying produce speed up
    flyingSpeedUp :: Float
  }

-- | Possible inputs for player
data PlayerInput
  = -- | manages, if player flyes now or not
    PlayerFly Bool
  | -- | activates player shield for exact input
    PlayerShield

-- | Player's size settings
playerHeight :: Float
playerHeight = screenMaxY / 13

playerWidth :: Float
playerWidth = screenMaxX / 13

-- | Shield settings
defaultShieldTime :: Float
defaultShieldTime = 3

defaultShieldCooldown :: Float
defaultShieldCooldown = 15

-- | speed settings
maxSpeedUp :: Float
maxSpeedUp = 0.5

speed :: Float
speed = 1000

-- | functions helpers
isShieldUp :: PlayerState -> Bool
isShieldUp state = (shieldTime state) > 0

getMaxShieldReload :: PlayerState -> Float
getMaxShieldReload = const defaultShieldCooldown

getCurrentShieldReload :: PlayerState -> Float
getCurrentShieldReload = shieldCooldown

-- | updates player state caused by input
recieveControll :: PlayerInput -> PlayerState -> PlayerState
recieveControll (PlayerFly isFlying') state = state {isFlying = isFlying'}
recieveControll PlayerShield state =
  case shieldCooldown state of
    0 ->
      state
        { shieldTime = defaultShieldTime,
          shieldCooldown = defaultShieldCooldown
        }
    _ -> state

-- | get bounds of player model, that should be compared to detect collisions
getCollider :: PlayerState -> Collider
getCollider state = CircleCollider (point state) playerHeight

-- | Initial state
initPlayer :: PlayerState
initPlayer =
  PlayerState
    { point = (-0.4 * screenMaxX, 0 * screenMaxY),
      shieldTime = 0,
      shieldCooldown = 0,
      isFlying = False,
      flyingSpeedUp = 0
    }

-- | Update player's state with time
updatePlayer :: Float -> PlayerState -> PlayerState
updatePlayer dt state =
  state
    { point = (x, newPointY),
      shieldCooldown = newShieldCooldown,
      shieldTime = newShieldTime,
      flyingSpeedUp = clippedSpeedUp
    }
  where
    newShieldCooldown = updateShieldTimings shieldCooldown
    newShieldTime = updateShieldTimings shieldTime
    updateShieldTimings :: (PlayerState -> Float) -> Float
    updateShieldTimings getShieldProperty = max ((getShieldProperty state) - dt) 0
    clippedSpeedUp = if abs newPointY == screenMaxY then 0 else newSpeedUp
    newSpeedUp = case isFlying state of
      True -> min (flyingSpeedUp state + dt) maxSpeedUp
      False -> max (flyingSpeedUp state - dt) (- maxSpeedUp)
    -- We compare abs of our new value of Y with screen size to not exceed screen bounds
    -- and muptiply by the sign of it to determine if coordinate above or below zero
    newPointY = (min (abs (y + yChange)) screenMaxY) * signum (y + yChange)
    yChange = dt * speed * newSpeedUp
    (x, y) = point state

-- | Picture of out character
hero :: Picture
hero = color green $ circleSolid playerHeight

shield :: Picture
shield = color yellow $ circleSolid (playerHeight + 0.3)

-- | Drawing function
draw :: PlayerState -> Picture
draw state = translate x y $ hero <> shieldRender
  where
    shieldRender
      | shieldTime state == 0 = blank
      | shieldTime state < 1 && isBlinking = blank
      | shieldTime state < 1 && not isBlinking = shield
      | otherwise = shield
    isBlinking = floor ((shieldTime state) * 10) `mod` 2 == 1
    (x, y) = point state
