module GlobalStateHandlers
  ( withDeathStats,
    withScaledDraw,
  )
where

import Graphics.Gloss.Interface.IO.Game
import Screen


type GameOf state =
  state ->
  (state -> Picture) ->
  (Event -> state -> state) ->
  (Float -> state -> state) ->
  IO ()

data GameStatus a = Alive a [a] | Dead a [a]

-- | Provides a Death state to the game
-- and a death screen with statistics and restart
withDeathStats :: (a -> Bool) 
               -> [a]
               -> (a -> [String]) 
               -> GameOf (GameStatus a) 
               -> GameOf a
withDeathStats isAlive tryAgainLevels getStats wrapper initial draw input time =
  wrapper (Alive initial tryAgainLevels) newDraw newInput newTime
  where
    -- Just call the old draw when alive
    newDraw (Alive a _) = draw a
    -- And draw the death screen when dead
    newDraw (Dead a _) =
      color (light black) (rectangleSolid screenWidth screenHeight)
        <> (color white $ scale 0.7 0.7 $ text "You died")
        <> ( translate 0 (-50) $
               color white $
                 scale 0.3 0.3 $ text "Press Enter"
           )
        -- And stats
        <> ( translate 0 (-150) $
               color white $
                 scale 0.2 0.2 $
                   pictures $
                    zipWith (translate 0)
                      (map (*145) [0..])
                      (map text $ getStats a)
           )

    -- redirect input or restart game on Enter when dead
    newInput event (Alive a levels) = Alive (input event a) levels
    newInput (EventKey (SpecialKey KeyEnter) Down _ _) (Dead _ (next:queue)) =
        Alive next queue 
    newInput _ dead = dead

    newTime dt (Alive a levels) =
      if isAlive newState
        then Alive newState levels
        else (Dead a levels)
      where
        newState = time dt a
    newTime _dt dead = dead



-- | Run a game with scaled by (scaleX, scaleY) draw 
withScaledDraw :: Float -> Float -> GameOf a -> GameOf a
withScaledDraw scaleX scaleY wrapper initial draw =
  wrapper initial newDraw
  where
    newDraw = (scale scaleX scaleY . draw)
