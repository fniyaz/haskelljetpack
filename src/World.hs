{-# LANGUAGE OverloadedStrings #-}

module World where

import Geometry
import Graphics.Gloss
import Obstacles
import Collectable
import Screen

import Control.Monad.Random
import RandomTools


-- | Fantom types for Template data type
data Absolute

data Relative

-- | Part of the game a player has to pass through
data Template t = Template
  { templateBackground :: Picture,
    templateXCoordinate :: Float,
    templateLength :: Float,
    templateObstacles :: [Obstacle]
  }

-- | World state to be interacted
data WorldState = WorldState
  { worldTemplates :: [Template Relative]
  }

-- Templates initialization

-- | Creates a template given background, width, and list of obstacles
createTemplate :: Picture -> Float -> [Obstacle] -> Template Relative
createTemplate background xSize obstacles =
  Template
    { -- shifts to the right to start background at logical zero
      templateBackground = translate (xSize / 2) 0 background,
      templateXCoordinate = 0,
      templateLength = xSize,
      templateObstacles = obstacles
    }

-- | Initial template without obstacles to make user acquanted with gameplay
initialTemplate :: Template Relative
initialTemplate = createTemplate background xSize []
  where
    (xSize, ySize) = (screenWidth * 1.5, screenHeight)
    col = light aquamarine
    instruction = scale 0.3 0.3 $ text "Press Space to fly. Press Tab to immunity"
    background = color col (rectangleSolid xSize ySize) <> instruction

-- | Available templates to choose the next template from
sampleTemplates :: [Template Relative]
sampleTemplates = zipWith3 createTemplate backgrounds xSizes obstacles
  where
    transformPoint :: (Float, Float) -> Point -> Point
    transformPoint (xSize, ySize) (x, y) = (x * xSize, (y - 0.5) * ySize)
    createScaledLazer :: (Float, Float) -> (Point, Point) -> Obstacle
    createScaledLazer (xSize, ySize) ((x1, y1), (x2, y2)) =
      createLazer (transformPoint (xSize, ySize) (x1, y1)) (transformPoint (xSize, ySize) (x2, y2))

    xSizes = [xSize1, xSize2, xSize3] -- width of templates
    backgrounds = [background1, background2, background3] -- backgounds of templates
    obstacles = [obstacles1, obstacles2, obstacles3] -- obstacles belong to templates
    (xSize1, ySize1) = (screenWidth * 2, screenHeight)
    background1 = color blue (rectangleSolid xSize1 ySize1)
    lazerPoints1 =
      [ ((0.1, 0.3), (0.1, 0.5)),
        ((0.2, 0.5), (0.2, 0.94)),
        ((0.25, 0.1), (0.25, 0.3)),
        ((0.4, 0.05), (0.4, 0.6)),
        ((0.52, 0.34), (0.52, 0.78)),
        ((0.6, 0.2), (0.6, 0.4)),
        ((0.6, 0.56), (0.6, 0.68)),
        ((0.78, 0.12), (0.78, 0.34)),
        ((0.78, 0.73), (0.78, 0.96)),
        ((0.93, 0.06), (0.93, 0.5))
      ]
    coins1 = map (fromCollectable . coinAt . transformPoint (xSize1, ySize1)) 
      [ (0.1, 0.5)
      , (0.3, 0.75)
      , (0.7, 0.5)
      , (0.9, 0.25)
      ]
    obstacles1 = coins1 ++ map (createScaledLazer (xSize1, ySize1)) lazerPoints1

    (xSize2, ySize2) = (screenWidth * 3, screenHeight)
    background2 = translate 0 (-ySize2 / 4)
                  (color (dark red) (rectangleSolid xSize2 (ySize2 / 2)))
               <> translate 0 (ySize2 / 4)
                  (color (dark green) (rectangleSolid xSize2 (ySize2 / 2)))
               <> translate 0 0
                  (color white (rectangleSolid xSize2 (ySize2 / 7)))
    lazerPoints2 =
      [ ((0.04, 0.09), (0.16, 0.09)),
        ((0.24, 0.92), (0.38, 0.92)),
        ((0.12, 0.2), (0.24, 0.2)),
        ((0.15, 0.8), (0.33, 0.8)),
        ((0.3, 0.45), (0.6, 0.45)),
        ((0.4, 0.72), (0.57, 0.72)),
        ((0.5, 0.87), (0.7, 0.87)),
        ((0.62, 0.13), (0.8, 0.13)),
        ((0.75, 0.6), (0.9, 0.6)),
        ((0.8, 0.8), (0.97, 0.8))
      ]
    coins2 = map (fromCollectable . coinAt . transformPoint (xSize2, ySize2))
      [ (x / 10, sin $ (/10) $ (*pi) $ x) | x <- [1..9]]
    obstacles2 = coins2 ++ map (createScaledLazer (xSize2, ySize2)) lazerPoints2

    (xSize3, ySize3) = (screenWidth * 2.5, screenHeight)
    background3 = color (light black) (rectangleSolid xSize3 ySize3)
    lazerPoints3 =
      [ ((0.04, 0.09), (0.16, 0.09)),
        ((0.2, 0.5), (0.2, 0.94)),
        ((0.12, 0.2), (0.24, 0.2)),
        ((0.4, 0.05), (0.4, 0.6)),
        ((0.3, 0.45), (0.6, 0.45)),
        ((0.6, 0.2), (0.6, 0.4)),
        ((0.5, 0.87), (0.7, 0.87)),
        ((0.62, 0.13), (0.8, 0.13)),
        ((0.75, 0.6), (0.9, 0.6)),
        ((0.8, 0.8), (0.97, 0.8))
      ]
    coins3 = map (fromCollectable . coinAt . transformPoint (xSize3, ySize3)) 
      [ (0.5, 0.5) ]
    obstacles3 = coins3 ++ map (createScaledLazer (xSize3, ySize3)) lazerPoints3

-- | Slightly disturb template
templateRandomaze :: (RandomGen g) => Template Relative -> Rand g (Template Relative)
templateRandomaze template = do
  let obstacles = templateObstacles template
  distubanceX <- getRandomRs (-disturbanceFactor, disturbanceFactor)
  distubanceY <- getRandomRs (-disturbanceFactor, disturbanceFactor)

  return $ template {
    templateObstacles = zipWith moveObstacle 
                                (zip distubanceX distubanceY) 
                                obstacles
  }
  where
    disturbanceFactor = screenHeight / 5


-- | Infinite list of templates
templatesList :: (RandomGen g) => Rand g [Template Relative]
templatesList = wrapSplit $ do
      indexes <- getRandomRs (0, length sampleTemplates - 1)
      let orderedTemplates = map ((!!) sampleTemplates) indexes

      sequence $ map templateRandomaze orderedTemplates


-- | Generate endless level using the StgGen random generator
initWorld :: (RandomGen g) => Rand g WorldState
initWorld = do
  templates <- templatesList

  return $ WorldState (initialTemplate : templates)

-- From relative to absolute

-- | Transform obstacles with coordinates relative to the template
--    given offset over X to obstacles with absolute coordinates
toAbsoluteObstacles :: Float -> [Obstacle] -> [Obstacle]
toAbsoluteObstacles offsetX = map (moveObstacle (offsetX, 0))

-- | Transform templates from relative to absolute representation given offset
toAbsolute :: Float -> [Template Relative] -> [Template Absolute]
toAbsolute _offsetX [] = []
toAbsolute offsetX (template : templates) = absoluteTemplate : toAbsolute (newX + len) templates
  where
    newX = templateXCoordinate template + offsetX
    len = templateLength template

    obstacles = templateObstacles template

    absoluteTemplate = template {templateXCoordinate = newX, templateObstacles = toAbsoluteObstacles newX obstacles}

-- Screen presentation

-- | Whether the template is on screen
onScreen :: Template Absolute -> Bool
onScreen template = x + len > - screenMaxX && x < screenMaxX
  where
    x = templateXCoordinate template
    len = templateLength template

-- | Templates that are on screen given list of relative templates
getOnScreenTemplates :: [Template Relative] -> [Template Absolute]
getOnScreenTemplates templates = takeWhile onScreen (dropWhile (not . onScreen) (toAbsolute (- screenMaxX) templates))


spanOnScreenRelative  :: [Template Relative]
                    -> ([Template Relative], [Template Relative], [Template Relative])
spanOnScreenRelative templates = (fst <$> left, fst <$> mid, fst <$> right)
  where
    absolutes = toAbsolute (- screenMaxX) templates
    (left, midRight) = span (not . onScreen . snd) (zip templates absolutes)
    (mid, right) = span (onScreen . snd) midRight

-- Drawing

-- | Render obstacles with absolute coordinates
drawTemplateObstacles :: [Obstacle] -> Picture
drawTemplateObstacles obstacles = pictures (map drawObstacle obstacles)

-- | Render template in absolute coordinates
drawTemplate :: Template Absolute -> Picture
drawTemplate template = translate offset 0 background <> drawTemplateObstacles obstacles
  where
    offset = templateXCoordinate template
    background = templateBackground template
    obstacles = templateObstacles template

-- | Render list of templates
drawTemplates :: [Template Relative] -> Picture
drawTemplates templates = pictures (map drawTemplate (getOnScreenTemplates templates))

-- | Draw current world that fits to the screen
drawWorld :: WorldState -> Picture
drawWorld (WorldState templates) =
  drawTemplates templates

-- Update world

-- | Update template given current horizontal speed and time passed
updateTemplate :: Float -> Float -> Template Relative -> Template Relative
updateTemplate speed dt template = template {templateXCoordinate = (x - dt * speed)}
  where
    x = templateXCoordinate template

-- | Update list of active templates given current horizontal speed and time passed
updateTemplates :: Float -> Float -> [Template Relative] -> [Template Relative]
updateTemplates _speed _dt [] = []
updateTemplates speed dt (template : templates) = updateTemplate speed dt template : templates

-- | Update the world using current horizontal speed and the passed time
-- And handle all it's colliders
updateWorld :: Float -> Float -> WorldState -> WorldState
updateWorld speed dt world = world {worldTemplates = updateTemplates speed dt (worldTemplates world)}

-- Intercation with colliders

-- | Get all the on-screen obstacles and their colliders
getObstacleColliders :: WorldState -> [(Obstacle, Collider)]
getObstacleColliders (WorldState templates) =
  zip onScreenObs (map getObstacleCollider obstacles)
  where
    obstacles = (concat . map templateObstacles . getOnScreenTemplates) templates
    (_, onScreenTmps, _) = spanOnScreenRelative templates
    onScreenObs = (concat . map templateObstacles) onScreenTmps

removeObstacle :: Obstacle -> WorldState -> WorldState
removeObstacle obstacle world = world {
  worldTemplates = clean $ worldTemplates world
}
  where
    clean templates = 
      left ++ (map remove mid) ++ right
      where
        (left, mid, right) = spanOnScreenRelative templates

        remove tmp = tmp {
          templateObstacles = filter (/= obstacle) (templateObstacles tmp)
        }

removeObstacles :: [Obstacle] -> WorldState -> WorldState
removeObstacles = flip (foldr removeObstacle)
