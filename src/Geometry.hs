module Geometry where

import CommonDraws
import Data.Function ((&))
import Graphics.Gloss hiding (Circle)

data Collider
  = -- | collider at the given coordinate with redius
    CircleCollider Point Float
  | -- | collider at the given (left, bottom) and (right, top) coordinate with rotation
    BoxCollider Point Point Float

--type Point = (Float, Float)
type Polygon = [Point]

data Circle = Circle Point Float

data Line
  = Sloped {lineSlope, lineYIntercept :: Float}
  | Vert {lineXIntercept :: Float}
  deriving (Show)

-- change coords of all ponts by (x,y)
colTranslated :: Point -> Collider -> Collider
colTranslated (deltaX, deltaY) (CircleCollider (x, y) radius) =
  CircleCollider (x + deltaX, y + deltaY) radius
colTranslated (deltaX, deltaY) (BoxCollider (x1, y1) (x2, y2) angle) =
  BoxCollider (x1 + deltaX, y1 + deltaY) (x2 + deltaX, y2 + deltaY) angle

-- change angle of rotation
colRotated :: Float -> Collider -> Collider
colRotated angle (BoxCollider (x1, y1) (x2, y2) _) = (BoxCollider (x1, y1) (x2, y2) angle)
colRotated _ myCircle = id myCircle

-- | Check if two colliders are colliding
-- | Here we do not translate, but do rotation inside
colliding :: Collider -> Collider -> Bool
colliding (CircleCollider (x1, y1) radius1) (CircleCollider (x2, y2) radius2) =
  circlesColliding (x1, y1) radius1 (x2, y2) radius2
colliding (BoxCollider (x1, y1) (x2, y2) angle1) (BoxCollider (x3, y3) (x4, y4) angle2) =
  boxesColliding (x1, y1) (x2, y2) angle1 (x3, y3) (x4, y4) angle2
colliding (BoxCollider (x2, y2) (x3, y3) angle1) (CircleCollider (x1, y1) radius) =
  boxCircleColliding (x2, y2) (x3, y3) angle1 (x1, y1) radius
colliding (CircleCollider (x1, y1) radius) (BoxCollider (x2, y2) (x3, y3) angle1) =
  boxCircleColliding (x2, y2) (x3, y3) angle1 (x1, y1) radius

-- check if 2 circles are colliding
circlesColliding :: Point -> Float -> Point -> Float -> Bool
circlesColliding (x1, y1) radius1 (x2, y2) radius2 = not (distance > radius2 + radius1)
  where
    distance = coordinatesDistance (x1, y1) (x2, y2)

-- check if  2 polygons (rectangles) are colliding
boxesColliding :: Point -> Point -> Float -> Point -> Point -> Float -> Bool
boxesColliding (x1, y1) (x2, y2) angle1 (x3, y3) (x4, y4) angle2 =
  -- checks if each point from polygon1 lies in polygon2
  elem
    True
    ( [foreach polygon1 polygon2 inPolygon]
        -- checks in reverse :
        -- if each point from polygon1 lies in polygon2
        ++ [foreach polygon2 polygon1 inPolygon]
    )
  where
    polygon1 = makePolygon (x1, y1) (x2, y2) angle1
    polygon2 = makePolygon (x3, y3) (x4, y4) angle2

-- check if box and circle are colliding
boxCircleColliding :: Point -> Point -> Float -> Point -> Float -> Bool
boxCircleColliding (x3, y3) (x4, y4) angle2 (x1, y1) radius =
  elem
    True
    ( [foreach myPolygon (Circle (x1, y1) radius) inCircle] ++ [inPolygon (x1, y1) myPolygon]
        ++ [foreach (polygonSides myPolygon) (Circle (x1, y1) radius) intersectCircleSide]
    )
  where
    myPolygon = makePolygon (x3 - radius, y3 - radius) (x4 + radius, y4 + radius) angle2

-- For each element of the first list applies function f
-- with arguments : element (x), second list (xs2)
foreach :: [a] -> b -> (a -> b -> Bool) -> Bool
foreach (x : xs) xs2 f = (f x xs2) && (foreach xs xs2 f)
foreach [] _ _ = False

-- from 2 points and an angle of rotation constructs coords for a polygon
-- rotation begins from left down point (that is always 1st in the BoxCollider notation )
makePolygon :: Point -> Point -> Float -> Polygon
makePolygon (x1, y1) (x2, y2) angle =
  [(x1, y1)]
    ++ [rotatePointAroundAPoint (x1, y1) (x1, y2) angle]
    ++ [rotatePointAroundAPoint (x1, y1) (x2, y2) angle]
    ++ [rotatePointAroundAPoint (x1, y1) (x2, y1) angle]

-- rotates a point (x,y) around point (ox, oy) by angle
rotatePointAroundAPoint :: Point -> Point -> Float -> Point
rotatePointAroundAPoint (ox, oy) (x, y) angle = (px, py)
  where
    px = cosa * (x - ox) - sina * (y - oy) + ox
    py = sina * (x - ox) + cosa * (y - oy) + oy
    cosa = cos (angle)
    sina = sin (angle)

-- get sides (Point, Point) from points
polygonSides :: Polygon -> [(Point, Point)]
polygonSides poly@(p1 : ps) = zip poly $ ps ++ [p1]
polygonSides [] = []

-- checks if some of the sides of polygon intersect a circle
intersectCircleSide :: (Point, Point) -> Circle -> Bool
intersectCircleSide ((x3, y3), (x4, y4)) (Circle (x1, y1) radius) =
  radius < coordinatesDistance (x1, y1) (xM, yM) && between xM x3 x4
  where
    -- line containing this side
    lineOfSide = carrier ((x3, y3), (x4, y4))
    -- intersection point of rectangle side and perpendicular ray from circle center
    (xM, yM) = coordXIntersec lineOfSide

    coordXIntersec (Sloped a b) = (xx, yy)
      where
        xx = (x1 + a * y1 - a * b) / (a * a + 1)
        yy = a * xx + b
    coordXIntersec (Vert xint) = (xint, y1)

-- calculates distance between 2 coordinates
coordinatesDistance :: Point -> Point -> Float
coordinatesDistance (ax, ay) (bx, by) =
  sqrt ((ax - bx) ** 2 + (ay - by) ** 2)

-- checks if the point inside a circle
inCircle :: Point -> Circle -> Bool
inCircle (px, py) (Circle (centerX, centerY) radius) =
  (px - centerX) ** 2 + (py - centerY) ** 2 <= radius ** 2

-- This code (from intersects till inPolygon) was borrowed from
-- https://github.com/cambraca/haskell_point_polygon

-- @intersects (px, py) l@ is true if the ray {(x, py) | x ≥ px} intersects l.
intersects :: Point -> Line -> Bool
intersects (px, _) (Vert xint) = px <= xint
intersects (px, py) (Sloped m b)
  | m < 0 = py <= m * px + b
  | otherwise = py >= m * px + b

-- checks if a pont is on line
onLine :: Point -> Line -> Bool
{- Is the point on the line? -}
onLine (px, _) (Vert xint) = px == xint
onLine (px, py) (Sloped m b) = py == m * px + b

-- Finds the line containing the given line segment
carrier :: (Point, Point) -> Line
carrier ((ax, ay), (bx, by))
  | ax == bx = Vert ax
  | otherwise = Sloped slope yint
  where
    slope = (ay - by) / (ax - bx)
    yint = ay - slope * ax

-- checks if a number (x) between a and b
between :: Ord a => a -> a -> a -> Bool
between x a b
  | a > b = b <= x && x <= a
  | otherwise = a <= x && x <= b

-- checks if a point inside polygon
inPolygon :: Point -> Polygon -> Bool
inPolygon p@(px, py) = f 0 . polygonSides
  where
    f n [] = odd n
    f n (side : sides)
      | far = f n sides
      | onSegment = True
      | rayIntersects = f (n + 1) sides
      | otherwise = f n sides
      where
        far = not $ between py ay by
        onSegment
          | ay == by = between px ax bx
          | otherwise = p `onLine` line'
        rayIntersects =
          intersects p line'
            && (py /= ay || by < py)
            && (py /= by || ay < py)
        ((ax, ay), (bx, by)) = side
        line' = carrier side

-- | For debug purposes
drawCollider :: Collider -> Picture
drawCollider (CircleCollider (x, y) radius) = translate x y $ thickCircle radius 2
drawCollider (BoxCollider (x1, y1) (x2, y2) angle) =
  thickRect dx dy 1
    & translate (dx / 2) (dy / 2)
    & rotate angle
    & translate x1 y1
  where
    dy = (y2 - y1)
    dx = (x2 - x1)
