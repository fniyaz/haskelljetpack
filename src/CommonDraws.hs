module CommonDraws (thickRect, drawMenu, transparent) where

import Graphics.Gloss

import Data.Function ((&))

import Screen


transparent :: Color
transparent = makeColor 0 0 0 0


thickRect :: Float -> Float -> Float -> Picture
thickRect width height theta = top <> bottom <> left <> right
  where
    top    = rectangleSolid width theta & translate 0 (height / 2)
    bottom = rectangleSolid width theta & translate 0 (-height / 2)
    left   = rectangleSolid theta height & translate (-width / 2) 0
    right  = rectangleSolid theta height & translate (width / 2) 0


drawMenu :: Color -> [String] -> Picture
drawMenu bgColor xs = translate (-screenMaxX) (screenMaxY) menuWithBg
  where
    menuWithBg = bgBox <> translate 0 (-35) menu

    boxWidth = fromIntegral $ 40 * foldr max 0 (map length xs)
    boxHeight = 50 * fromIntegral (length xs)

    bgBox = color bgColor $ translate (0) (-boxHeight/2)
       $ rectangleSolid (boxWidth) boxHeight

    menu = pictures $ zipWith drawLower menuLines yOffsets

    drawLower pic y = translate 0 y pic

    -- Y coodrdinate for each menu line from top of the menu
    yOffsets = map (*(-50)) [0..]
    menuLines = map (scale 0.3 0.3 . text) xs
